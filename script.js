'use strict';

function hideSplashScreen() {
    let page = document.getElementById("page-splash");
    let NoScrollClass = document.getElementById('body');
    page.hidden = true;
    NoScrollClass.classList.remove("no-scroll");
}

function showSplashScreen() {
    let page = document.getElementById("page-splash");
    let NoScrollClass = document.getElementById('body');
    page.hidden = false;
    NoScrollClass.classList.add("no-scroll");
}

function createCommentElement(comment) {
    let element = document.createElement('div');
    let content = '<a href="#" class="muted">' + user.name + '</a>' + '<p>' + comment.text + '</p>';
    element.innerHTML = content;
    element.className = "py-2 pl-3";
    return element;
}

function createCommentElement2(user) {
    let com = document.querySelector('[name=com]').value;

    if (com === undefined || com.trim().length === 0)
        return;

    let div = document.createElement('div');
    let content = '<a href="#" class="muted">' + user.name + '</a>' + '<p>' + com + '</p>';
    div.innerHTML = content;
    div.className = "py-2 pl-3";
    return div;
}

function createPostElement(post) {
    let div = document.createElement('div');
    const html = `
  <div>
    <img class="d-block w-100" src="${post.image}" alt="Post image">
  </div>
  
  <div class="px-4 py-3">
    <div class="d-flex justify-content-around">
      <span class="h1 mx-2 muted">
        <i class="far fa-heart"></i>
      </span>
      <span class="h1 mx-2 muted">
        <i class="far fa-comment"></i>
      </span>
      <span class="mx-auto"></span>
      <span class="h1 mx-2 muted">
        <i class="far fa-bookmark"></i>
      </span>
    </div>
  <hr>
  <div>
    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsum ad est cumque nulla voluptatem enim voluptas minima illum quis! Voluptatibus dolorem minus tempore aliquid corrupti nesciunt, obcaecati fuga natus officiis.</p>
    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsum ad est cumque nulla voluptatem enim voluptas minima illum quis! Voluptatibus dolorem minus tempore aliquid corrupti nesciunt, obcaecati fuga natus officiis.</p>
    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsum ad est cumque nulla voluptatem enim voluptas minima illum quis! Voluptatibus dolorem minus tempore aliquid corrupti nesciunt, obcaecati fuga natus officiis.</p>
  </div>
  <hr>
  <div id = "comment">
    <div class="py-2 pl-3">
      <a href="#" class="muted">someusername</a>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsum ad est cumque nulla voluptatem enim voluptas minima illum quis! Voluptatibus dolorem minus tempore aliquid corrupti nesciunt, obcaecati fuga natus officiis.</p>
    </div>
  <div class="py-2 pl-3">
      <a href="#" class="muted">someusername</a>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsum ad est cumque nulla voluptatem enim voluptas minima illum quis! Voluptatibus dolorem minus tempore aliquid corrupti nesciunt, obcaecati fuga natus officiis.</p>
  </div>
  </div>
  </div>
  `;
    div.innerHTML = html;
    return div;
}

let user = {
    id: 1,
    name: "Liverpool",
    login: "LFC_YNWA",
    password: "12345qwerty",
    isAuthorized: false
}
console.log(user)

let someUser = {
    id: 10,
    name: "someusername",
    login: "?",
    password: "?",
    isAuthorized: false
}

const image = document.querySelector('[name=image]').value

let post = {
    id: '1',
    author_name: 'someusername',
    //image: "https://placekitten.com/700/700"
    image: image
};
console.log(post)

let comment = {
    id: 1,
    post_id: post.id,
    user_name: user.name,
    user_id: user.id,
    text: "Fresh post. Lorem ipsum dolor sit amet consectetur adipisicing elit. Reprehenderit iste nemo nam pariatur ab eos temporibus, numquam expedita laboriosam? Repellat!",
    like: 1
}
console.log(comment)


function addComment() {
    document.getElementById("comment").append(createCommentElement(comment));
}

function addComment2() {
    document.getElementById("comment").append(createCommentElement2(someUser));
}

function addPost() {
    document.getElementById("post").append(createPostElement(post));
}

let like = document.getElementById('like');
like.addEventListener('click', function () {
    let heart = like.children[0];
    if (heart.classList.contains("text-grey")) {
        heart.classList.remove("text-grey", "fas");
    } else {
        heart.classList.add("text-grey", "fas");
    }
});

let bookmark = document.getElementById('bookmark');
bookmark.addEventListener('click', function () {
    let icon = bookmark.children[0];
    if (icon.classList.contains("fas")) {
        icon.classList.remove("fas");
    } else {
        icon.classList.add("fas");
    }
});

let postImage = document.getElementsByClassName("image")[0];
postImage.addEventListener('dblclick', function () {
    let heart = like.children[0];
    if (heart.classList.contains("text-danger")) {
        heart.classList.remove("text-danger", "fas");
    } else {
        heart.classList.add("text-danger", "fas");
    }
});

const form = document.getElementById("postForm");

form.addEventListener('submit', function (e) {
    e.preventDefault();

    const formData = new FormData(this);
    fetch('http://localhost:8080/posts', {
        method: 'POST',
        body: formData
    }).then(function (response) {
        return response.json();
    })
});


function showCommentForm() {
    let commentForm = document.getElementById("commentForm");
    if (commentForm.style.visibility === "visible") {
        commentForm.style.visibility = "hidden";
    } else {
        commentForm.style.visibility = "visible";
    }
}

const commentForm = document.getElementById("commentForm")
form.addEventListener('submit', function (e) {
    e.preventDefault();
    const commentFormData = new FormData(this);
    fetch('http://localhost:8080/comments', {
        method: 'POST',
        body: formData
    }).then(function (response) {
        return response.json();
    })
});


const users = fetch("http://localhost:8080/getUser")
    console.log(users)